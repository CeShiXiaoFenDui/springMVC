package com.pengtu.springMvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pengtu.springMvc.entity.AcctUser;
import com.pengtu.springMvc.service.UserService;

/** 
 * @author zl 
 * @version 创建时间：2017年3月22日 下午6:19:04 
 * 类说明 
 */
@Controller
@RequestMapping("/user")
public class UserController {

	private static final Logger LOGGER = Logger.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@RequestMapping("/showInfo")
	public String showUserInfo(ModelMap modelMap,HttpServletRequest request){
		String userId = request.getParameter("userId");
		System.out.println("id:"+userId);
		LOGGER.info("查询用户：" + userId);
		AcctUser userInfo = userService.load(userId);
		modelMap.addAttribute("nickName", "success.....");
		return "/user/showInfo";
	}
	
	@RequestMapping("/showInfos")
	public @ResponseBody List<AcctUser> showUserInfos(){
		LOGGER.info("查询用户全部用户");
		List<AcctUser> userInfos = userService.findAll();
		return userInfos;
	}
}
